/**
 * Created by chenfengjuan on 16/10/25.
 */
var GB2312UnicodeConverter={
    ToUnicode:function(str){
        var txt= escape(str).toLocaleLowerCase().replace(/%u/gi,'\\u');
        //var txt= escape(str).replace(/([%3F]+)/gi,'\\u');
        return txt.replace(/%7b/gi,'{').replace(/%7d/gi,'}').replace(/%3a/gi,':').replace(/%2c/gi,',').replace(/%27/gi,'\'').replace(/%22/gi,'"').replace(/%5b/gi,'[').replace(/%5d/gi,']').replace(/%3D/gi,'=').replace(/%20/gi,' ').replace(/%3E/gi,'>').replace(/%3C/gi,'<').replace(/%3F/gi,'?').replace(/%5c/gi,'\\');//
    }
    ,ToGB2312:function(str){
        if(str!=null) {
            return unescape(str.replace(/\\u/gi, '%u'));
        }else{
            return "";
        }
    }
}
function u2h(){
    var txtA = document.getElementById("json_input");
    var text = txtA.value;
    text = text.trim();
    // text = text.replace(/\u/g,"");
    txtA.value = GB2312UnicodeConverter.ToGB2312(text);
}

function h2u(){
    var txtA = document.getElementById("json_input");
    var text = txtA.value;
    text = text.trim();
    // text = text.replace(/\u/g,"");
    txtA.value = GB2312UnicodeConverter.ToUnicode(text);
}
