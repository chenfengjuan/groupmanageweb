/**
 * Created by chenfengjuan on 16/9/27.
 */
var bomH;
var activeTab=new Array();
var smA=new Object();//小助手管理信息的json
var $tabs=new Object();
var timer=0;
var userID;
var helperData=new Array();
var groupData=new Array();
var onlineFlag=0;
var addSmaFlag=0;
var keywordSecond=new Array();//群管理，一个群的关键字的第二个信息医院名字
$tabs.smAssistant= $("<li role='presentation' name='smAssistant' class='active smAssistant-tab'><a href='#'>小助手管理<span class='close'>x</span></a></li>");
$tabs.group= $("<li role='presentation' name='group' class='active group-tab'><a href='#'>群管理<span class='close'>x</span></a></li>");
$tabs.message= $("<li role='presentation' name='message' class='active message-tab'><a href='#'><lable>入群管理</lable><span class='close'>x</span></a></li>");
$tabs.keyword= $("<li role='presentation' name='keyword' class='active keyword-tab'><a href='#'>关键字管理<span class='close'>x</span></a></li>");
$tabs.assistantGroup= $("<li role='presentation' name='assistantGroup' class='active assistantGroup-tab'><a href='#'>小助手所在群<span class='close'>x</span></a></li>");
$tabs.dataOut= $("<li role='presentation' name='dataOut' class='active dataOut-tab'><a href='#'>数据导出<span class='close'>x</span></a></li>");
$().ready(function () {
    $.ajax({
        type: "GET",
        url: "http://wyeth.gemii.cc/HelperManage/isLogin",
        success: function (data) {
            if(data.status==-1){
              window.location.href="login.html";
            }else{
                userID=data.data.id;
                $("#userName").text(data.data.username);
                requestSma(true);
                //获取小助手下拉菜单内容
                $.ajax({
                    type: 'get',
                    url: "http://wyeth.gemii.cc/HelperManage/status/getRobotByUid?id="+userID,
                    data: "",
                    dataType: 'json',
                    success: function(data){
                        helperData=data.data;
                    }
                });
                timer=setInterval(requestSma,30000);
             }
        }
    });
    initIndex();
});
function initIndex() {
    bomH=$(window).height();
    $(document.body).css("height",bomH);
    $(window).resize(function() {
        $(document.body).css("height",$(window).height());
    });
    //左侧导航栏开关
    $("#parent-nav").on("click",switchNav);
    //默认tab页绑定点击事件
    $(".smAssistant-tab span").on("click",closeTab);
    //打开tab页
    $(".sidebar-child li").on("click",openTab);
    //激活tab页
    $(".nav-tabs").delegate("li","click",chooseTab);
    //打开消息子页面的内容
    $(".msg-child").delegate("li","click",openMsgChild);
    //群管理关键字修改绑定事件
    $("#groupTbody").delegate("a:nth-child(2)","click",modifyKey);
    //群管理 修改绑定事件
    $("#groupTbody").delegate("a:nth-child(1)","click",modifyGroup);
    //关键字修改页面修改关键字
    $("#groupKeyList").delegate("button","click",modifyGroupKey);
    //关键字修改页面"＋"的功能
    $(".key-height").delegate("button[name='add']","click",addOne);
    //关键字修改页面"－"的功能
    $(".key-height").delegate("button[name='delete']","click",deleteOne);
    //input输入验证不为空
    //$("#groupKeyList").delegate("input","blur",notNull);
    //$(".key-height").delegate("input","blur",notNull);
    //添加关键字
    $(".key-height").delegate("#keywordAdd","click",addKeys);
    $("select").on("change",function () {
        $(document).click();
    });
    $("#groupName").keydown(function (e) {
        // 回车键事件
        if(e.which == 13) {
            groupSearch();
        }
    });
    //删除小助手
    $("#smATbody").delegate(".deleteA a","click", function () {
        var help=$(this);
        firm(help);
    });
    //小助手扫码上线
    $("#smATbody").delegate("a.h5A","click",online);
    //上行关键字信息添加保存
    $("#msgTbody1").delegate("td>a.add","click",addSave);
    //上行关键字信息添加取消
    $("#msgTbody1").delegate("td>a.off","click",off);
    //小助手管理
    $("#robot").keydown(function (e) {
        // 回车键事件
        if(e.which == 13) {
            assSearch();
        }
    });
    //小助手管理，在线状态排序
    $("#online").on("change",assChange);
    //上行关键字修改入群状态
    $("#msgTbody1").delegate("select[name='groupStatus']","change",modifyUp);
    //单元格添加双击后，带三个省略号的文字打开关闭切换事件
    $("#msgTbody1").delegate("tr","dblclick",toggleTextOverflow);
    $("#msgTbody2").delegate("tr","dblclick",toggleTextOverflow);
    $(".pagination").delegate("li","click",jump);
}
function toggleTextOverflow() {
    $(this).find("td").toggleClass("textOverflow");
}
//上行关键字入群管理，修改入群状态
function modifyUp() {
    var text=$(this).find("option:selected").text();
    var data=new Object();
    data.messagestatus=$(this).find("option:selected").text();
    data.id=$(this).parent().parent().children().eq(0).attr("name");
    if (confirm("确定要修改入群状态吗？")) {
        //发送保存修改的请求
        $.ajax({
            type: "POST",
            url: "http://wyeth.gemii.cc/HelperManage/message/modifyMessage",
            data: data,
            dataType: "json",
            success: function (data) {
                var data= getMsg1Form(msg1PageNum);
                sendMsg1Form(data);
            }
        });
    }else{
        if(text=="手动邀请"){
            $(this).val("未入群");
        }else{
            $(this).val("手动邀请");
        }

    }
}
//h5入群管理，修改入群状态
function modifyH5() {
    var text=$(this).find("option:selected").text();
    var data=new Object();
    data.entergroupstatus=$(this).find("option:selected").text();
    data.id=$(this).parent().parent().children().eq(0).attr("name");
    if (confirm("确定要修改入群状态吗？")) {
        //发送保存修改的请求
        $.ajax({
            type: "POST",
            url: "http://wyeth.gemii.cc/HelperManage/message/modifyMessage2",
            data: data,
            dataType: "json",
            success: function (data) {
                var data= getMsg2Form(msg2PageNum);
                sendMsg2Form(data);
            }
        });
    }else{
        if(text=="手动邀请"){
            $(this).val("未入群");
        }else{
            $(this).val("手动邀请");
        }

    }
}
//小助手管理，在线状态排序
function assChange() {
    var status=$("#online option:selected").text();
    for (var i=0;i<$("#smATbody tr").length;i++){
        if($("#smATbody tr").eq(i).children().eq(3).text().indexOf(status)!=-1){
            $("#smATbody").prepend($("#smATbody tr").eq(i));
        }
    }
}
//小助手管理
function assSearch() {
    var robot=$("#robot").val();
    if(robot!=""){
        for (var i=0;i<$("#smATbody tr").length;i++){
            if($("#smATbody tr").eq(i).children().eq(0).text().indexOf(robot)!=-1){
                $("#smATbody").prepend($("#smATbody tr").eq(i));
            }
        }
    }
}
//上行关键字信息添加取消
function off() {
    $(this).parent().parent().remove();
    if(!$("#msgTbody1 tr td:first-child").find("input").length>0){
        $(".msg1 thead tr th:last-child").addClass("none-box");
        $(".msg1 tr td:last-child").addClass("none-box");
    }
}
//小助手扫码上线
function online() {
    clearInterval(timer);
    $("#smAcode").addClass("none-box");
    $("#QRcode").removeClass("none-box").parent().addClass("overflowH");
    var tag=$(this).parent().parent().find("td").eq(2).text();
    onlineFlag=0;
    requestOnline(tag);
}
var onlineTimer=0;
function requestOnline(tag) {
    $.ajax({
        type: "get",
        url: "http://wyeth.gemii.cc/HelperManage/user/restartRobot?robotTag=" +tag+"&clerkid="+userID,
        async:true,
        success: function (data) {
            if(data.data!=null){
                $("#onlineImg").attr("src",data.data);
                $("#onlineImg").attr({"width":"200","height":"200"});
                $("#Code >div").removeClass("ball-spin-fade-loader");
                if($("#QRcode .tip").hasClass("none-box")){
                    $("#QRcode .tip").removeClass("none-box");
                }
                if(!$("#QRcode .red").hasClass("none-box")){
                    $("#QRcode .red").addClass("none-box");
                }
                $("#onlineImg").removeClass("none-box");
                onlineTimer=setInterval(function () {
                    isOnline(tag);
                },2000);
            }else {
                if(!$("#QRcode .tip").hasClass("none-box")){
                    $("#QRcode .tip").addClass("none-box");
                }
                if($("#QRcode .red").hasClass("none-box")){
                    $("#QRcode .red").removeClass("none-box");
                }
            }
            setTimeout(function () {
                if (data.data == null) {
                    if(onlineFlag<3){
                        onlineFlag++;
                        requestOnline(tag);
                    }else{
                        $("#Code >div").removeClass("ball-spin-fade-loader");
                        $("#QRcode .red").text("加载失败，请返回重试");
                    }
                }
            }, 3000);
        }
    });
}
//检查小助手是否上线
function isOnline(tag) {
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/status/getRobotByUid",
        data: {
            "id":userID
        },
        dataType: 'json',
        success: function(data){
            // console.log(data.data);//status
            $.each(data.data,function (i, item) {
                if(item.robottag==tag){
                    if(item.status=="online"){
                        clearInterval(onlineTimer);
                        backsmA();
                    }
                }
            });
        }
    });

}
//上行关键字信息添加保存
function addSave() {
    var addTr=$(this).parent().parent().children();
    var re=/[20|][0-9][0-9][-][0-9]{0,1}[0-9][-][0-9]{0,1}[0-9]\s*[0-2]{0,1}[0-9][:][0-5]{0,1}[0-9][:][0-5]{0,1}[0-9]/;
    if(addTr.eq(0).find("input").val()==""){
        alert("昵称不能为空");
    }else if(addTr.eq(1).find("input").val()==""){
        alert("消息内容不能为空");
    }else if(addTr.eq(1).find("input").val().split(/[+＋]/).length!=3){
        alert("消息内容格式错误");
    }else {
        if (confirm("保存后不能再删除，确定保存吗？")) {
            var roommsg1=$(this).parent().parent().children().eq(3).find("select option:selected").val();
            var td=$(this).parent();
            td.find("select[name='groupStatus']").parent().css("padding","0");
            var data=new Object();
            data.username=td.parent().children().eq(0).find("input").val();
            data.upmessage=td.parent().children().eq(1).find("input").val();
            data.remindmessage=td.parent().children().eq(2).find("input").val();
            data.roomName=td.parent().children().eq(3).find("select").val();
            data.messagestatus=td.parent().children().eq(4).text();
            data.messagingtime=td.parent().children().eq(5).find("input").val();
            data.matchstatus=td.parent().children().eq(6).find("select").val();
            data.roomid=roommsg1;
            data.robotid=robotMsG1;
            //发送保存修改的请求
            console.log(data);
            $.ajax({
                type: "POST",
                url: "http://wyeth.gemii.cc/HelperManage/message/insertMessage",
                data: data,
                dataType: "json",
                success: function (data) {
                    var data=getMsg1Form(msg1PageNum);
                    sendMsg1Form(data);
                }
            });
        }
    }
}
//添加一条上行关键字信息
function addMsg() {
    $(".msg1 tr th:last-child").removeClass("none-box");
    $(".msg1 tr td:last-child").removeClass("none-box");
    $("#msgTbody1").prepend(
        "<tr><td><input type='text' class='form-control' placeholder='输入昵称'>"+
        "</td><td><input type='text' class='form-control' placeholder='上海+虹口妇幼+20161101'>"+
        "</td><td><input type='text' class='form-control' placeholder='输入提醒消息'>"+
        "</td><td><select class='show-tick form-control' name='groupNa'> </select>"+
        "</td><td>手动邀请"+
        "</td><td><input type='text' class='form-control datainp' placeholder='年-月-日 时:分:秒'>"+
        "</td><td><select class='show-tick form-control'>" +
        "<option selected>群匹配成功</option> <option>匹配全国群</option> <option>匹配城市群</option> <option>未匹配到群</option></select>"+
        "</td><td><a class='add'>保存</a>&nbsp;&nbsp;<a class='off'>取消</a></td></tr>"
    );
    $("#msgTbody1 .datainp").jeDate({
        format:"YYYY-MM-DD  hh:mm:ss",
        isTime:true,
        ishmsVal:false,
        maxDate:$.nowDate(0),        //最大日期
        zIndex:3000
    });
    $.each(groupData,function (i, item) {
        if(i==0){
            $("#msgTbody1 select[name='groupNa']").append("<option selected value="+item.roomID+">"+item.roomName+"</option>");
        }else{
            $("#msgTbody1 select[name='groupNa']").append("<option value="+item.roomID+">"+item.roomName+"</option>");
        }
    });
    $("#msgTbody1 select[name='groupNa']").children().eq(0).attr("selected","true");
    $(".msg1 tbody tr:first-child").children().has("select").css("padding", "0");
    $(".msg1 tbody tr:first-child").children().has("input").css("padding", "0");
}
//弹出一个询问框，有确定和取消按钮
function firm(help) {
    //利用对话框返回的值 （true 或者 false）
    if (confirm("确定删除吗？")) {
        $.ajax({
            type: "GET",
            url: "http://wyeth.gemii.cc/HelperManage/user/deleteRobot?clerkid="+userID+"&robottag="+help.parent().parent().find("td").eq(2).text(),
            success: function (data) {
                if(data.status==200){
                    requestSma();
                }
            }
        });
    }

}
//小助手管理刷新
function refash() {
    requestSma();
}
//请求小助手信息
function requestSma(first) {
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/status/getRobotByUid",
        data: {
            "id":userID
        },
        dataType: 'json',
        success: function(data){
            var flag=true;//如果reload页面，小助手列表在这里不需要重新渲染，reload时会渲染
            //新增加或删除了小助手
            if((first==undefined)&&(first!=true)){
                if(data.data.length!=helperData.length){
                    location.reload();
                    flag=false;
                }
            }
            if(flag){
                $("#smATbody").html("");
                $.each(data.data, function (i, item) {
                    var str = "";
                    if (item.status == "offline") {
                        str = "点击扫码上线";
                    }
                    $("#smATbody").append(
                        "<tr><td>" + item.robotname + "</td>" +
                        "<td>" + item.robotid + "</td>" +
                        "<td>" + item.robottag + "</td>" +
                        "<td><span>" + item.status + "</span><span>&nbsp;&nbsp;</span></span><a class='h5A'>" + str + "</a></td>" +
                        "<td>" + checkNull(item.h5Url) + "</td>" +
                        "<td class='deleteA'><a>删除</a></td>" +
                        "</tr>");
                });
                if ($("#robot").val() != "") {
                    assSearch();
                }
            }
        }
    });
}
//点击扫码上线,返回
function backsmA() {
    clearInterval(onlineTimer);
    $("#smAcode").removeClass("none-box");
    $("#QRcode").addClass("none-box").parent().removeClass("overflowH");
    $("#Code >div").addClass("ball-spin-fade-loader");
    if($("#QRcode .tip").hasClass("none-box")){
        $("#QRcode .tip").removeClass("none-box");
    }
    if(!$("#QRcode .red").hasClass("none-box")){
        $("#QRcode .red").text("正在重新加载，请稍后...");
        $("#QRcode .red").addClass("none-box");
    }
    $("#onlineImg").attr("src","");
    $("#onlineImg").addClass("none-box");
    requestSma();
    timer=setInterval(requestSma,30000);

}
//添加小助手
function addHelper() {
    clearInterval(timer);
    $("#smAcode").addClass("none-box");
    $("#add").removeClass("none-box").parent().addClass("overflowH");
    addSmaFlag=0;
    requestAddAsm();
}
var addSmaTimer=0;
function requestAddAsm() {
    $.ajax({
        type: "GET",
        async:true,
        url: "http://wyeth.gemii.cc/HelperManage/user/addRobot?clerkid="+userID,
        success: function (data) {
            if(data.data!=null){
                $("#addImg").attr("src",data.data);
                $("#addImg").attr({"width":"200","height":"200px"});
                $("#addCode >div").removeClass("ball-spin-fade-loader");
                if($("#add .tip").hasClass("none-box")){
                    $("#add .tip").removeClass("none-box");
                }
                if(!$("#add .red").hasClass("none-box")){
                    $("#add .red").addClass("none-box");
                }
                $("#addImg").removeClass("none-box");
                addSmaTimer=setInterval(isAddSma,2000);
            }else {
                if(!$("#add .tip").hasClass("none-box")){
                    $("#add .tip").addClass("none-box");
                }
                if($("#add .red").hasClass("none-box")){
                    $("#add .red").removeClass("none-box");
                }
            }
            setTimeout(function () {
                if (data.data == null) {
                    if(addSmaFlag<3){
                        addSmaFlag++;
                        requestAddAsm();
                    }else{
                        $("#addCode >div").removeClass("ball-spin-fade-loader");
                        $("#add .red").text("加载失败，请返回重试");
                    }
                }
            }, 3000);
        }
    });
}
//检查是否添加了小助手
function isAddSma() {
    $.ajax({
        type: "POST",
        url: "http://wyeth.gemii.cc/HelperManage/status/getRobotByUid",
        data: {
            "id":userID
        },
        dataType: 'json',
        success: function(data){
            // if(data.data!=null){
                if(data.data.length!=helperData.length){
                    clearInterval(addSmaTimer);
                    addFinish();
                }
            // }
        }
    });
}
//已确认添加，返回
function addFinish() {
    $("#smAcode").removeClass("none-box");
    $("#add").addClass("none-box").parent().removeClass("overflowH");
    $("#addCode >div").addClass("ball-spin-fade-loader");
    if($("#add .tip").hasClass("none-box")){
        $("#add .tip").removeClass("none-box");
    }
    if(!$("#add .red").hasClass("none-box")){
        $("#add .red").text("正在重新加载，请稍后...");
        $("#add .red").addClass("none-box");
    }
    $("#addImg").attr("src","");
    $("#addImg").addClass("none-box");
    requestSma();
    timer=setInterval(requestSma,30000);
}
//关键字修改页面"＋"的功能
function addOne() {
    $(".key-height").prepend(" <form class='form-inline keyModify' role='form' onsubmit='return false;'> " +
        "<input type='text' disabled='true' class='form-control' placeholder='城市' > " +
        "<input type='text' class='form-control' placeholder='医院' > " +
        "<input type='text' disabled='true' class='form-control' placeholder='开始时间'> " +
        "<input type='text' disabled='true' class='form-control' placeholder='结束时间'> " +
        // "<button type='button' class='btn btn-default' name='add'>＋</button> " +
        // "<button type='button' class='btn btn-default' name='delete'>－</button> " +
        "</form>");
    var key=$("#groupKeyList").find("form").eq(0).find("input");
    $(".key-height").find("input").eq(0).val(key.eq(0).val());
    $(".key-height").find("input").eq(2).val(key.eq(2).val());
    $(".key-height").find("input").eq(3).val(key.eq(3).val());
}
//关键字修改页面"－"的功能
function deleteOne() {
    var eLength=$(".key-height").children("form").length;
    if(eLength>1){
        $(this).parent().remove();
    }else {
        $(".key-height").addClass("none-box");
    }
}
//群管理，关键字修改
function modifyKey() {
    if ($(this).text()=="关键字管理"){
        var groupCode=$(this).parent().parent().find("td").eq(0).text();//关键字对应的群code
        var keyGroup=$(this).parent().parent().find("td").eq(1).text();//关键字对应的群名称
        $.ajax({
            type: "POST",
            url: "http://wyeth.gemii.cc/HelperManage/keyword/getKeywordByGid",
            //url:"test/groupKey.json",
            data: {"id":groupCode},
            dataType: "json",
            success: function (data) {
                if(data.data.length>0){
                    if(!$("#error").hasClass("none-box")){
                        $("#error").addClass("none-box");
                    }
                    $(".con-box").not(".none-box").addClass("none-box");
                    $("#keyModify").removeClass("none-box");
                    $("#groupKeyList").html("");//清空groupKeyList内容
                    keywordSecond.splice(0,keywordSecond.length);
                    $(".key-height").addClass("none-box");
                    $(".key-height").find("input").val("");
                    $.each(data.data,function (i, item) {
                        keywordSecond.push(item.keyword);
                        $("#groupKeyList").append(
                            "<form class='form-inline keyModify' role='form'><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.city)+
                            "><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.keyword)+
                            "><input disabled='true' type='text' placeholder='开始时间' class='form-control marginL-3' value="+checkNull(item.startTime)+
                            "><input disabled='true' type='text' placeholder='结束时间' class='form-control marginL-3' value="+checkNull(item.endTime)+
                            "><button type='button' class='btn btn-default' name="+item.id+">修改</button></form>"
                        );
                    });
                    $("#keyGroupName").text(keyGroup);
                    $("#keyGroupName").addClass(groupCode);
                }else{
                    alert("此群没有关键字");
                }
            }
        });
    }else if($(this).text()=="取消"){
        for( var i=3; i<10; i++) {
            var s = $(this).parent().parent().children().eq(i);
            var text=s.attr("name");
            s.remove("input").text(text);
        }
        $(this).parent().parent().children().css("padding","8px");
        $(this).text("关键字管理");
        $(this).prev().text("修改");

    }
}
//关键字修改页面修改关键字
var keyContentBefore=new Array();
var keyContentAfter=new Array();
function modifyGroupKey() {
    var inputDom=$(this).parent().find("input");
    var buttonName=$(this).attr("name");
    if(inputDom.eq(1).attr("disabled")=="disabled"){
        keyContentBefore[0]=inputDom.eq(0).val();
        keyContentBefore[1]=inputDom.eq(1).val();
        keyContentBefore[2]=inputDom.eq(2).val();
        keyContentBefore[3]=inputDom.eq(3).val();
        inputDom.eq(1).attr("disabled",false);
    }else {
        var flag=0;//关键字信息变更标识
        keyContentAfter[0]=inputDom.eq(0).val();
        keyContentAfter[1]=inputDom.eq(1).val();
        keyContentAfter[2]=inputDom.eq(2).val();
        keyContentAfter[3]=inputDom.eq(3).val();
        for(var i=0; i<4; i++){
            if(keyContentBefore[i]!=keyContentAfter[i]){
                flag++;
            }
        }
        var exist=0;//关键字信息存在标识
        if(flag>0){
            $.each(keywordSecond,function (i, item) {
                if(item==keyContentAfter[1]){
                    exist++;
                }
            });
        }
        if(keyContentAfter[1]!=""){
            if (flag > 0) {
                if(exist==0){
                    //发送关键字修改后的信息
                    $.ajax({
                        type: "POST",
                        url: "http://wyeth.gemii.cc/HelperManage/keyword/updateKeyword",
                        //url: "test/groupKey.json",
                        data: {
                            "city": keyContentAfter[0],
                            "keyword": keyContentAfter[1],
                            "startTime": keyContentAfter[2],
                            "endTime": keyContentAfter[3],
                            "id": buttonName
                        },
                        dataType: "json",
                        success: function () {
                            inputDom.eq(1).attr("disabled", true);
                            for(var i=0;i<keywordSecond.length;i++){
                                if(keywordSecond[i]==keyContentBefore[1]){
                                    keywordSecond[i]=keyContentAfter[1];
                                }
                            }
                        }
                    });
                }else{
                    alert("关键字已存在。");
                }
            } else {
                inputDom.eq(1).attr("disabled", true);
                //alert("关键字已存在。");
            }
        }else {
            alert("修改的关键字不能为空");
        }
    }
}
//打开关键字添加部分
function openAddKey() {
    $(".key-height").removeClass("none-box");
    $(".key-height").html("");
    addOne();
    $(".key-height").append("<div class='keyModify addBefore'> " +
        "<button type='button' class='btn btn-default' id='keywordAdd'>添加</button> " +
        "</div>");
    if(!$("#error").hasClass("none-box")){
        $("#error").addClass("none-box");
    }

}
//添加所创建的关键字
function addKeys() {
    var groupCreateKeys=new Array();//批量添加关键字
    var formLength=$(".key-height").children("form").length;
    for(var i=0; i<formLength; i++){
        var createKey=new Object();
        createKey.key1=$(".key-height").children("form").eq(i).children("input").eq(0).val();
        createKey.key2=$(".key-height").children("form").eq(i).children("input").eq(1).val();
        createKey.key3=$(".key-height").children("form").eq(i).children("input").eq(2).val();
        createKey.key4=$(".key-height").children("form").eq(i).children("input").eq(3).val();
        if(createKey.key2!=""){
            groupCreateKeys[i]=createKey;
        }
    }
    if(groupCreateKeys.length>0){
        var exist=0;
        $.each(keywordSecond,function (i, item) {
            for(var j=0;j<groupCreateKeys.length;j++){
                if(item==groupCreateKeys[j].key2){
                    exist++;
                }
            }
        });
        if(exist==0){
            $("#keywordAdd").off("click",addKeys);
            for(var i=0; i<groupCreateKeys.length; i++){
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "http://wyeth.gemii.cc/HelperManage/keyword/insertKeyword",
                    data: {
                        "city":groupCreateKeys[i].key1,
                        "keyword":groupCreateKeys[i].key2,
                        "startTime":groupCreateKeys[i].key3,
                        "endTime":groupCreateKeys[i].key4,
                        "roomid":$("#keyGroupName").attr("class")
                    },
                    dataType: "json",
                    success: function () {
                        $.ajax({
                            type: "POST",
                            url: "http://wyeth.gemii.cc/HelperManage/keyword/getKeywordByGid",
                            //url:"test/groupKey.json",
                            data: {"id":$("#keyGroupName").attr("class")},
                            dataType: "json",
                            success: function (data) {
                                $("#groupKeyList").html("");//清空groupKeyList内容
                                keywordSecond.splice(0,keywordSecond.length);
                                $.each(data.data,function (i, item) {
                                    keywordSecond.push(item.keyword);
                                    $("#groupKeyList").append(
                                        "<form class='form-inline keyModify' role='form'><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.city)+
                                        "><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.keyword)+
                                        "><input disabled='true' type='Date' class='form-control marginL-3' value="+checkNull(item.startTime)+
                                        "><input disabled='true' type='Date' class='form-control marginL-3' value="+checkNull(item.endTime)+
                                        "><button type='button' class='btn btn-default' name="+item.id+">修改</button></form>"
                                    );
                                });
                            }
                        });
                        $(".key-height").html("");
                    }
                });
            }
            $("#keywordAdd").on("click",addKeys);
        }else{
            alert("添加的关键字已存在");
        }

    }else {
        alert("添加关键字不能为空。");
    }
}
//群管理 修改绑定事件
function modifyGroup() {
    if($(this).text()=="修改"){
        $(this).text("保存");
        $(this).next().text("取消");
        for( var i=3; i<10; i++) {
            var s = $(this).parent().parent().children().eq(i);
            var text=s.text();
            s.text("");
            s.attr("name",text);
            s.append("<input type='text' class='form-control' value="+text+">");
        }
        $(this).parent().parent().children().has("input").css("padding","0");
    } else{
        var self=$(this);
        var data=new Object();
        data.roomid=$(this).parent().parent().children().eq(0).text();
        data.area=$(this).parent().parent().children().eq(3).find("input").val();
        data.province=$(this).parent().parent().children().eq(4).find("input").val();
        data.city=$(this).parent().parent().children().eq(5).find("input").val();
        data.department=$(this).parent().parent().children().eq(6).find("input").val();
        data.subname=$(this).parent().parent().children().eq(7).find("input").val();
        data.channel=$(this).parent().parent().children().eq(8).find("input").val();
        data.alias=$(this).parent().parent().children().eq(9).find("input").val();
        //发送保存修改的请求
        $.ajax({
            type: "POST",
            url: "http://wyeth.gemii.cc/HelperManage/group/updateGroupInfo",
            data: data,
            dataType: "json",
            success:function () {
                for( var i=3; i<10; i++) {
                    var s = self.parent().parent().children().eq(i);
                    var text=s.find("input").val();
                    s.remove("input").text(text);
                }
                self.parent().parent().children().css("padding","8px");
            }
        });
       // saveModify("http://wyeth.gemii.cc/HelperManage/group/updateGroupInfo",data);
        $(this).text("修改");
        $(this).next().text("关键字管理");
    }

}
//左侧导航栏开关
function switchNav() {
    if($(".sidebar-child").not(".msg-child").hasClass("none-box")){
        $(".sidebar-child").removeClass("none-box");
        $(".caret").removeClass("nav-open");
    }else{
        $(".sidebar-child").addClass("none-box");
        $(".caret").addClass("nav-open");
    }
}
//激活tab页
function chooseTab() {
    var lengthArray=activeTab.length;
    activeTab[lengthArray]=$(".nav-tabs .active");
    //console.log(activeTab);
    $(".active")
        .removeClass("active");
    $(".close").addClass("none-box");
    var liName=$(this).attr("name");
    $(this)
        .addClass("active")
        .find("span").filter(".close")
        .removeClass("none-box");

    $(".con-box").not(".none-box").addClass("none-box");
    $("div[id^="+liName+"]").removeClass("none-box");

    $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    $("a[id^="+liName+"]").addClass("sidebar-click");
    if(liName=="message"){
        var w=$("li[name='message']").find("a").find("lable").text();
        if(w=="H5-1"){
            $(".con-box").not(".none-box").addClass("none-box");
            $("div[id^='message3']").removeClass("none-box");
            $("a[id^='h5']").removeClass("sidebar-click");
            $("#h5-1").addClass("sidebar-click");
        }else if(w=="H5入群"){
            $(".con-box").not(".none-box").addClass("none-box");
            $("div[id^='message2']").removeClass("none-box");
            $("a[id^='h5']").removeClass("sidebar-click");
            $("#h5-2").addClass("sidebar-click");
        }else {
            $(".con-box").not(".none-box").addClass("none-box");
            $("div[id^='message1']").removeClass("none-box");
            $("a[id^='h5']").removeClass("sidebar-click");
            $("#h5-3").addClass("sidebar-click");
        }
    }
    smTab();//如果小助手页面为活动页，小助手页面复原，开启定时器，否则关闭定时器
}
//小助手页面复原，开启定时器，否则关闭定时器
function smTab() {
    if(!$("#smAssistant-mngm").hasClass("none-box")){
        $("#smAcode").removeClass("none-box");
        $("#QRcode").addClass("none-box").parent().removeClass("overflowH");
        $("#Code >div").addClass("ball-spin-fade-loader");
        $("#onlineImg").attr("src","");
        $("#onlineImg").addClass("none-box");
        $("#add").addClass("none-box").parent().removeClass("overflowH");
        $("#addCode >div").addClass("ball-spin-fade-loader");
        $("#addImg").attr("src","");
        $("#addImg").addClass("none-box");
        if($("#QRcode .tip").hasClass("none-box")){
            $("#QRcode .tip").removeClass("none-box");
        }
        if(!$("#QRcode .red").hasClass("none-box")){
            $("#QRcode .red").text("正在重新加载，请稍后...");
            $("#QRcode .red").addClass("none-box");
        }
        if($("#add .tip").hasClass("none-box")){
            $("#add .tip").removeClass("none-box");
        }
        if(!$("#add .red").hasClass("none-box")){
            $("#add .red").text("正在重新加载，请稍后...");
            $("#add .red").addClass("none-box");
        }
        requestSma();
        timer=setInterval(requestSma,30000);
    }else {
        clearInterval(timer);
    }
}
//关闭tab页
function closeTab() {
    if($(".nav-tabs li").length>1) {
        var lengthArray = activeTab.length;
        $(this).parent().parent().remove();
        $(".con-box").not(".none-box").addClass("none-box");
        var liName = activeTab[lengthArray - 1].attr("name");
        while($(".nav-tabs").has("li[name="+liName+"]").length==0){
            activeTab.splice(lengthArray - 1,1);
            lengthArray = activeTab.length;
            liName = activeTab[lengthArray - 1].attr("name");
        }
        activeTab[lengthArray - 1].addClass("active")
            .find(".close")
            .removeClass("none-box");
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $("a[id^=" + liName + "]").addClass("sidebar-click");
        if(liName=="message"){
            var str=activeTab[lengthArray - 1].find("a").find("lable").text();
            if(str=="上行关键字"){
                $("div[id^='message1']").removeClass("none-box");
                $("#h5-3").addClass("sidebar-click");
            }else if(str=="H5入群"){
                $("div[id^='message2']").removeClass("none-box");
                $("#h5-2").addClass("sidebar-click");
            }
        }else{
            $("div[id^=" + liName + "]").removeClass("none-box");
        }
        activeTab.splice(lengthArray - 1, 1);
    }else{
        activeTab.splice(0,activeTab.length);
        $(".nav-tabs li").remove();
        $(".con-box").not(".none-box").addClass("none-box");
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    }
    smTab();//如果小助手页面为活动页，小助手页面复原，开启定时器，否则关闭定时器
}
//打开相应tab页
function getTab(tabName,self) {
    var tab=tabName+"-tab";
    if(tabName=="message"){
        $(".msg-child").toggleClass("none-box","");
    }else{
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(self).addClass("sidebar-click");
        if($(".nav-tabs").find("."+tab).length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[tabName]);
            intTb(tabName);
            $("."+tab+" span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $("."+tab)
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^="+tabName+"]").removeClass("none-box");
    }
}
//初始化列表
function intTb(tabName) {
    if(tabName=="keyword"){
        initKeywordTb();//初始化关键字管理
    }else if(tabName=="group"){
        initGroupTb();//初始化群管理
    }else if(tabName=="assistantGroup"){
        initInGroupTb();//初始化小助手所在群
    }
}
//打开tab页
function openTab(event) {
    var lengthArray=activeTab.length;
    activeTab[lengthArray]=$(".nav-tabs .active");
    var tabName=event.target.id.split("-")[0];
    getTab(tabName,this);
    smTab();//如果小助手页面为活动页，小助手页面复原，开启定时器，否则关闭定时器

}
//打开消息子页面
function openMsgChild() {
    clearInterval(timer);
    $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    $(this).addClass("sidebar-click");
    $(this).parent().prev().addClass("sidebar-click");
    if ($(".nav-tabs").find(".message-tab").length <= 0) {
        $(".active")
            .removeClass("active")
            .find(".close").addClass("none-box");
        $(".nav-tabs").append($tabs["message"]);
        $(".message-tab span").on("click", closeTab);
    } else {
        $(".active")
            .removeClass("active")
            .find(".close")
            .addClass("none-box");
        $(".message-tab")
            .addClass("active")
            .find(".close")
            .removeClass("none-box");
    }
    $(".con-box").addClass("none-box");
    if(event.target.id=="h5-1") {
        $("#msgAssistant3").html("");
        initMsgTb3();//初始化消息管理
        $("li[name='message']").find("a").find("lable").text("H5-1");
        $("div[id^='message3']").removeClass("none-box");
    }
    if(event.target.id=="h5-2") {
        $("#msgAssistant2").html("");
        initMsgTb2();//初始化消息管理
        $("li[name='message']").find("a").find("lable").text("H5入群");
        $("div[id^='message2']").removeClass("none-box");
    }
    if(event.target.id=="h5-3") {
        $("#msgAssistant1").html("");
        initMsgTb1();//初始化消息管理
        $("li[name='message']").find("a").find("lable").text("上行关键字");
        $("div[id^='message1']").removeClass("none-box");
    }
}

//关键字管理页面
//关键字列表的页码
var keywordPageNum=0;
//关键字页面取得表单信息
function getKeywordForm(pages) {
    var formData=new Object();
    formData.robotID=$("#selectAssis-key").val();  //获取Select选择的Value;
    formData.city=$("#keyword1").val();
    formData.keyword=$("#keyword2").val();
    formData.edc=$("#edcTime").val();
    formData.page=pages;
    formData.pageSize=20;
    if($("#selectAssis-key :selected").text()=="All"){
        formData.type="all";
    }else{
        formData.type="";
    }
   // console.log(formData);
    return formData;
}
//提交表单
function sendKeywordForm(formData) {
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc//HelperManage/keyword/selectKeyword",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#keywordTbody").html("");//清空keywordTbody内容
            $.each(data.data.keywords, function (i, item) {
                $("#keywordTbody").append(
                    "<tr><td>" + item.roomid + "</td><td>" + checkNull(item.robotName) +
                    "</td><td>" + checkNull(item.city) + "</td><td>" + checkNull(item.keyword)+
                    "</td><td>" + checkNull(item.startTime) +" - "+checkNull(item.endTime) + "</td><td>" +item.roomName+ "</td></tr>");
            });
            $("#totalPages").text(data.data.total);
            if(data.data.total>0){
                $("#p1").text(keywordPageNum);
            }else {
                $("#p1").text(0);
            }
            $("#p2").text(data.data.count);
            initPages("page-keyword",data.data.count,keywordPageNum);
           // alert("搜索成功了！");
        }
    });
}
//关键字管理，提交表单，处理返回结果
function keywordSearch() {
    keywordPageNum=1;
    var data= getKeywordForm(keywordPageNum);
    sendKeywordForm(data);

}
//初始化加载全部关键字信息的第一页
function initKeywordTb(){
    //初始化下拉菜单
    $("#selectAssis-key").html("");
    $.each(helperData, function(i, item) {
        $("#selectAssis-key").append(
            "<option value="+item.robotid+">"+item.robotname+"</option>");
    });
    $("#selectAssis-key").prepend(
        "<option value="+userID+">All</option>");
    $("#selectAssis-key option:first-child").attr("selected", true);
    $("#selectAssis-key").selectpicker('refresh');
    $("#selectAssis-key").on('change',keywordSearch);
    //配置时间控件
    $("#edcTime").jeDate({
        ishmsVal:false,
        format:"YYYY-MM-DD",
        zIndex:3000,
    })
    //初始化关键字列表
    keywordSearch();

}
//关键字上一页
function prevKeyPage() {
    if(keywordPageNum>1) {
        keywordPageNum--;
        var data= getKeywordForm(keywordPageNum);
        sendKeywordForm(data);
        //alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//关键字下一页
function nextKeyPage() {
    if(keywordPageNum<parseInt($("#p2").text())) {
        keywordPageNum++;
        var data= getKeywordForm(keywordPageNum);
        sendKeywordForm(data);
        //alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}

//消息管理页面
//消息列表的页码
var robotMsG1;
var msg1PageNum=0;
//取得消息搜索表单
function getMsg1Form(pages) {
    var formData=new Object();
    formData.robotID=$("#msgAssistant1").val();  //获取Select选择的Value;
    formData.upMessage=$("#message1").val();
    formData.username=$("#nickname1").val();
    if($("#msgStatus1").val()=="All"){
        formData.status="";
    }else {
        formData.status=$("#msgStatus1").val();
    }
    formData.page=pages;
    formData.pageSize=20;
    return formData;
}
//消息子页面请求群名列表，用于修改
function requestG(robotID) {
    $.ajax({
        type: 'get',
        url: "http://wyeth.gemii.cc/HelperManage/group/getGroupByRobot?page=1&type=&pageSize=100&groupName=&robotID="+robotID,
        data: "",
        dataType: 'json',
        success: function(data){
            groupData=data.data.groups;
        }
    });
}
//发送表单
function sendMsg1Form(formData) {
    robotMsG1=formData.robotID;
    requestG(robotMsG1);
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/message/selectMessage",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#msgTbody1").html("");//清空内容
            $.each(data.data.messages, function (i, item) {
                var enter=checkNull(item.messagestatus);
                if((enter=="未入群")||(enter=="手动邀请")){
                    var enter2="";
                    if(enter=="未入群"){
                        enter2="手动邀请";
                    }else{
                        enter2="未入群";
                    }
                    enter="<select class='show-tick form-control'" +
                        " name='groupStatus'><option selected>"+enter+"</option> <option>"+enter2+"</option> </select>";
                }
                $("#msgTbody1").append(
                    "<tr><td class='textOverflow'  name="+item.id+">"+checkNull(item.username)+
                    "</td><td  class='textOverflow'>"+checkNull(item.upmessage)+
                    "</td><td  class='textOverflow'>"+checkNull(item.remindmessage)+
                    "</td><td  class='textOverflow'>"+checkNull(item.roomName)+
                    "</td><td  class='textOverflow'>"+enter+
                    "</td><td  class='textOverflow'>"+checkNull(item.messagingtime).split(".")[0]+
                    "</td><td  class='textOverflow'>"+checkNull(item.matchstatus)+
                    "</td><td class='textOverflow'></td></tr>"
                );
            });
            $("select[name='groupStatus']").parent().css("padding","0");
            if(!$("#msgTbody1 tr td:first-child").find("input").length>0){
                $(".msg1 thead tr th:last-child").addClass("none-box");
                $(".msg1 tr td:last-child").addClass("none-box");
            }
            $("#totalPages-msg1").text(data.data.total);
            if(data.data.total>0){
                $("#p1-msg1").text(msg1PageNum);
            }else {
                $("#p1-msg1").text(0);
            }
            $("#p2-msg1").text(data.data.count);
            initPages("page-msg1",data.data.count,msg1PageNum);
            //alert("搜索成功了！");
        }
    });
}

//消息管理，提交表单，处理返回结果
function msgSearch1() {
    msg1PageNum=1;
    var data=getMsg1Form(msg1PageNum);
    sendMsg1Form(data);
}
//初始化加载全部消息管理信息的第一页
function initMsgTb1(){
    $('#msgStatus1').selectpicker({
        maxOptions: 1
    });
    //初始化下拉菜单
    $("#msgAssistant1").html("");
    $.each(helperData, function(i, item) {
        $("#msgAssistant1").append(
            "<option value="+item.robotid+">"+item.robotname+"</option>");
    });
    $("#msgAssistant1 option:first-child").attr("selected", true);
    $("#msgAssistant1").selectpicker('refresh');
    $("#msgAssistant1").on('change',msgSearch1);
    $("#msgStatus1").on('change',msgSearch1);
    //初始化消息列表
    msgSearch1();

}
//消息管理上一页
function prevMsg1Page() {
    if(msg1PageNum>1) {
        msg1PageNum--;
        var data= getMsg1Form(msg1PageNum);
        sendMsg1Form(data);
        //alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }

}
//消息管理下一页
function nextMsg1Page() {
    if(msg1PageNum<parseInt($("#p2-msg1").text())) {
        msg1PageNum++;
        var data= getMsg1Form(msg1PageNum);
        sendMsg1Form(data);
        //alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}

//消息管理页面
//消息列表的页码
var msg2PageNum=0;
//message2里的robotid
var robotMsG2;
//取得消息搜索表单
function getMsg2Form(pages) {
    var formData=new Object();
    formData.robotID=$("#msgAssistant2").val();  //获取Select选择的Value;
    formData.username=$("#nickname2").val();
    formData.roomName="";
    formData.urlStatus="";
    if($("#enterStatus2").val()=="All"){
        formData.enterRoomStatus="";
    }else {
        formData.enterRoomStatus=$("#enterStatus2").val();
    }
    formData.code=$("#code2").val();
    formData.page=pages;
    formData.pageSize=20;
    //console.log(formData);
    return formData;
}
//转换表情标签
function emojiTo(arg){
    if((arg.indexOf("<span")>-1)&&(arg.indexOf("</span>")>-1)){
        var jsxDom="";
        var str1=arg.split("></span>");
        $.each(str1,function (i, item) {
            if((item.indexOf("<span")>-1)&&(item.indexOf("class")>-1)){
                var str2=item.split("<span");
                if(str2[0].indexOf("class=")>-1){
                    var classN=str2[0].replace(" class=","").replace('"',"").replace('"',"");
                    if((jsxDom=="")&&(str2[1]=="")){
                        jsxDom="<span class='"+classN+"'></span>";
                    }else{
                        jsxDom="<span>"+jsxDom+"<span class='"+classN+"'></span>"+str2[1]+"</span>";
                    }
                }else if(str2[1]!=""){
                    var classN=str2[1].replace(" class=","").replace('"',"").replace('"',"");
                    if((str2[0]=="")&&(jsxDom=="")){
                        jsxDom="<span class='"+classN+"'></span>";
                    }else{
                        jsxDom="<span>"+jsxDom+str2[0]+"<span class='"+classN+"'></span></span>";
                    }
                }else{
                    jsxDom="<span>"+jsxDom+str2[1]+"</span>";
                }
            }else if(item!=""){
                jsxDom="<span>"+jsxDom+item+"</span>";
            }
        });
        return jsxDom;
    }
    return arg;
}
//发送表单
function sendMsg2Form(formData) {
    //console.log(formData);
    robotMsG2=formData.robotID;
    //消息子页面请求群名列表，用于修改
    requestG(robotMsG2);
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/message/selectMessage2",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#msgTbody2").html("");//清空msgTbody内容
            if(data.data.total>0) {
                $.each(data.data.messages, function (i, item) {
                    var str="";
                    if((item.city==null)&&(item.hospitalKeyword==null)&&(item.edc==null)){
                        str="";
                    }else{
                        str=checkNull(item.city)+"+"+checkNull(item.hospitalKeyword)+"+"+checkNull(item.edc);
                    }
                    var enter=checkNull(item.enterGroupStatus);
                    if((enter=="未入群")||(enter=="手动邀请")){
                        var enter2="";
                        if(enter=="未入群"){
                            enter2="手动邀请";
                        }else{
                            enter2="未入群";
                        }
                        enter="<select class='show-tick form-control'" +
                            " name='groupStatus'><option selected>"+enter+"</option> <option>"+enter2+"</option> </select>";
                    }
                    $("#msgTbody2").append(
                        "<tr><td class='textOverflow' name="+item.id+">" +emojiTo(checkNull(GB2312UnicodeConverter.ToGB2312(item.userName)))+
                        "</td><td class='textOverflow'>"+checkNull(item.nowRoomName)+
                        "</td><td class='textOverflow'>" +checkNull(item.phoneCode) +
                         "</td><td class='textOverflow'>" +enter +
                        "</td><td class='textOverflow'>" +checkNull(item.h5Status)+
                        "</td><td class='textOverflow'>" +str+
                        "</td><td class='textOverflow'>" +checkNull(item.remindMessage) +
                        "</td><td class='textOverflow'>" +checkNull(item.matchStatus)+
                        "</td><td class='textOverflow'>"+checkNull(item.formTime)+
                        "</td></tr>"
                    );
                });
                $("select[name='groupStatus']").parent().css("padding","0");
                $("select[name='groupStatus']").on("change",modifyH5);
            }
            $("#totalPages-msg2").text(data.data.total);
            if(data.data.total>0){
                $("#p1-msg2").text(msg2PageNum);
            }else {
                $("#p1-msg2").text(0);
            }
            $("#p2-msg2").text(data.data.count);
            initPages("page-msg2",data.data.count,msg2PageNum);
        }
    });
}
//消息管理，提交表单，处理返回结果
function msgSearch2() {
    msg2PageNum=1;
    var data=getMsg2Form(msg2PageNum);
    sendMsg2Form(data);
}
//初始化加载全部消息管理信息的第一页
function initMsgTb2(){
    //初始化下拉菜单
    $("#msgAssistant2").html("");
    $.each(helperData, function(i, item) {
        $("#msgAssistant2").append(
            "<option value="+item.robotid+">"+item.robotname+"</option>");
    });
    $("#msgAssistant2 option:first-child").attr("selected", true);
    $("#msgAssistant2").selectpicker('refresh');
    $("#msgAssistant2").on('change',msgSearch2);
    $('#enterStatus2').on('change',msgSearch2);
    //初始化消息列表
    msgSearch2();

}
//消息管理上一页
function prevMsg2Page() {
    if(msg2PageNum>1) {
        msg2PageNum--;
        var data= getMsg2Form(msg2PageNum);
        sendMsg2Form(data);
        //alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }

}
//消息管理下一页
function nextMsg2Page() {
    if(msg2PageNum<parseInt($("#p2-msg2").text())) {
        msg2PageNum++;
        var data= getMsg2Form(msg2PageNum);
        sendMsg2Form(data);
       // alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}

//消息管理页面
//消息列表的页码
var msg3PageNum=0;
//取得消息搜索表单
function getMsg3Form(pages) {
    var formData=new Object();
    formData.helper_id=$("#msgAssistant3").val();  //获取Select选择的Value;
    formData.username=$("#nickname3").val();
    formData.groupname=$("#groupN3").val();
    if($("#groupStatus3").val()[0]==undefined){
       formData.status="";
    }else {
        formData.status=$("#groupStatus3").val()[0];
    }
    if($("#enterStatus3").val()[0]==undefined){
        formData.enterStatus="";
    }else {
        formData.enterStatus=$("#enterStatus3").val()[0];
    }
    formData.page=msg3PageNum;
    formData.pageSize=20;
    //console.log(formData);
    return formData;
}
//发送表单
function sendMsg3Form(formData) {
    //console.log(formData);
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/message/selectMessage",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#msgTbody3").html("");//清空msgTbody内容
            if(data.data.total>0) {
                $.each(data.data.messages, function (i, item) {
                    //var linkStatus = "";
                    $("#msgTbody3").append(
                        "<tr><td>" + $("#msgAssistant3 :selected").text() +
                        "</td><td>" + item.upmessage +
                        "</td><td>" + item.messageprocessstatus +
                        "</td><td>" + item.remindmessage +
                        "</td><td>" + item.username +
                        "</td><td>" + item.messagestatus +
                        "</td><td>" + item.confirmstatus +
                        "</td><td>" + item.messagingtime +
                        "</td><td>" + " " + "</td></tr>"
                    );
                });
            }
            $("#totalPages-msg3").text(data.data.total);
            if(data.data.total>0){
                $("#p1-msg3").text(msg3PageNum);
            }else {
                $("#p1-msg3").text(0);
            }
            $("#p2-msg3").text(data.data.count);
           // alert("搜索成功了！");
        }
    });
}
//消息管理，提交表单，处理返回结果
function msgSearch3() {
    msg3PageNum=1;
    var data=getMsg3Form(msg3PageNum);
    sendMsg3Form(data);
}
//初始化加载全部消息管理信息的第一页
function initMsgTb3(){
    $('#groupStatus3').selectpicker({
        maxOptions: 1
    });
    $('#enterStatus3').selectpicker({
        maxOptions: 1
    });
    //初始化下拉菜单
    $("#msgAssistant3").html("");
    $.each(helperData, function(i, item) {
        $("#msgAssistant3").append(
            "<option value="+item.robotid+">"+item.robotname+"</option>");
    });
    $("#msgAssistant3 option:first-child").attr("selected", true);
    $("#msgAssistant3").selectpicker('refresh');
    //初始化消息列表
    msgSearch3();

}
//消息管理上一页
function prevMsg3Page() {
    if(msg3PageNum>1) {
        msg3PageNum--;
        var data= getMsg3Form(msg3PageNum);
        sendMsg3Form(data);
        //alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }

}
//消息管理下一页
function nextMsg3Page() {
    if(msg3PageNum<parseInt($("#p2-msg3").text())) {
        msg3PageNum++;
        var data= getMsg3Form(msg3PageNum);
        sendMsg3Form(data);
       // alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}


//群管理页面
//群列表的页码
var groupPageNum=0;
//群管理页面取得表单信息
function getGroupForm(pages) {
    var formData=new Object();
    formData.groupName=$("#groupName").val();
    formData.page=pages;
    formData.pageSize=20;
    formData.clerkID=userID;
    //console.log(formData);
    return formData;
}
//提交表单
function sendGroupForm(formData) {
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/group/getGroup",
        //url:"test/groupList.json",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#groupTbody").html("");//清空groupTbody内容
            $.each(data.data.groups, function (i, item) {
                $("#groupTbody").append(
                "<tr><td>"+item.roomID+
                "</td><td>"+item.roomName+
                "</td><td>"+item.userCount+
                "</td><td>"+checkNull(item.area)+
                "</td><td>"+checkNull(item.province)+
                "</td><td>"+checkNull(item.city)+
                "</td><td>"+checkNull(item.department)+
                "</td><td>"+checkNull(item.groupAttr)+
                "</td><td>"+checkNull(item.channel)+
                "</td><td>"+checkNull(item.alias)+
                "</td>" +
                // "<td><a>修改</a>&nbsp;&nbsp;<a>关键字管理</a></td>" +
                "</tr>"
                );
            });

            $("#totalPages-group").text(data.data.total);
            if(data.data.total>0){
                $("#p1-group").text(groupPageNum);
            }else {
                $("#p1-group").text(0);
            }
            $("#p2-group").text(data.data.count);

            //初始化分页列表
            initPages("page-group",data.data.count,groupPageNum);
            //alert("搜索成功了！");
        }
    });
}

//群管理，提交表单，处理返回结果
function groupSearch(number) {
    if((number==undefined)||(number==null)){
        groupPageNum=1;
    }else{
        groupPageNum=number;
    }
    var data= getGroupForm(groupPageNum);
    sendGroupForm(data);

}
//初始化加载全部群管理信息的第一页
function initGroupTb(){
    //初始化群管理列表
    groupSearch();
}
//群信息上一页
function prevGroupPage() {
    if(groupPageNum>1) {
        groupPageNum--;
        var data= getGroupForm(groupPageNum);
        sendGroupForm(data);
        //alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//群信息下一页
function nextGroupPage() {
    if(groupPageNum<parseInt($("#p2-group").text())) {
        groupPageNum++;
        var data= getGroupForm(groupPageNum);
        sendGroupForm(data);
       // alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}
//群管理，关键字修改返回群管理
function backGroup() {
    $("#keyGroupName").attr("class","");
    $(".con-box").not(".none-box").addClass("none-box");
    $("div[id^='group']").removeClass("none-box");
    $("#keyModify").addClass("none-box");
}

//初始化分页列表
function initPages(id, total, pageNum) {
    //清空列表
    var l=$("#"+id).find("li").length;
    if(l>2){
        for(var i=0; i<l-2;i++){
            if($("#"+id).find("li").length>2){
                $("#"+id).find("li").eq(1).remove();
            }
        }
    }
    var count=0;
    var start=1;
    if(total>10){
        count=10;
        if(pageNum-5>0){
            start=pageNum-5;
        }
        if(pageNum+4>total){
            count=total;
            start=count-9;
        }else {
            count=start+9
        }

    }else {
        count=total;
    }
    var str="";
    for(var i=start;i<count+1; i++){
        str+="<li><a href='#"+i+"'>"+i+"</a></li> "
    }
    $("#"+id).find("li").eq(0).after(str);
    $("#"+id).find("li").find("a[href='#"+pageNum+"']").parent().addClass("activeli");

    if(total<2){
        if(!$("#"+id).parent().hasClass("none-box")){
            $("#"+id).parent().addClass("none-box");
        }
    }else{
        if($("#"+id).parent().hasClass("none-box")){
            $("#"+id).parent().removeClass("none-box");
        }
    }
}
//分页上到按钮跳转页面
function jump() {
    var number=$(this).find("a").attr("href").replace("#","");//点击的页码
    if(number!=""){
        number=parseInt(number);
        var tableName= $(this).parent().attr("id").split("-")[1];
        if(tableName=="group"){
            groupSearch(number);
        }else if(tableName=="msg1"){
            msg1PageNum=number;
            var data=getMsg1Form(number);
            sendMsg1Form(data);
        }else if(tableName=="msg2"){
            msg2PageNum=number;
            var data=getMsg2Form(number);
            sendMsg2Form(data);
        }else if(tableName=="keyword"){
            keywordPageNum=number;
            var data= getKeywordForm(number);
            sendKeywordForm(data);
        }else if(tableName=="inGropu"){
            inGroupPageNum=number;
            var data= getinGroupForm(number);
            sendInGroupForm(data);
        }
    }
}



//小助手所在群页面
//小助手所在群列表页码
var inGroupPageNum=0;
//小助手所在群页面取得表单信息
function getinGroupForm(pages) {
    var formData=new Object();
    formData.robotID=$("#assis-group").val();  //获取Select选择的Value;
    formData.groupName=$("#groupSearch").val();
    formData.page=pages;
    formData.pageSize=20;
    if($("#assis-group :selected").text()=="All"){
        formData.type="all";
    }else{
        formData.type="";
    }
    return formData;
}
//提交表单
function sendInGroupForm(formData) {
    $.ajax({
        type: 'POST',
        url: "http://wyeth.gemii.cc/HelperManage/group/getGroupByRobot",
        data: formData,
        dataType: 'json',
        success: function(data){
            //console.log(data);
            $("#inGroupTbody").html("");//清空inGroupTbody内容
            $.each(data.data.groups, function (i, item) {
                $("#inGroupTbody").append(
                    "<tr><td>" + item.robotName +
                    "</td><td>" + item.status +
                    "</td><td>" + item.roomName +
                    // "</td><td>" + checkNull(item.enterTime) +
                    "</td><td>" + checkNull(item.roomID) +
                    "</td></tr>");
            });
            $("#totalPages-inGropu").text(data.data.total);
            if(data.data.total>0){
                $("#p1-inGropu").text(inGroupPageNum);
            }else {
                $("#p1-inGropu").text(0);
            }
            $("#p2-inGropu").text(data.data.count);

            initPages("page-inGropu",data.data.count,inGroupPageNum);
            //alert("搜索成功了！");
        }
    });
}
//小助手所在群，提交表单，处理返回结果
function inGroupSearch() {
    inGroupPageNum=1;
    var data= getinGroupForm(inGroupPageNum);
    sendInGroupForm(data);

}
//初始化加载小助手所在群信息的第一页
function initInGroupTb(){
    //初始化下拉菜单
    $("#assis-group").html("");
    $.each(helperData, function(i, item) {
        $("#assis-group").append(
            "<option value="+item.robotid+">"+item.robotname+"</option>");
    });
    $("#assis-group").prepend(
        "<option value="+userID+">All</option>");
    $("#assis-group option:first-child").attr("selected", true);
    $("#assis-group").selectpicker('refresh');
    $("#assis-group").on('change',inGroupSearch);
    //初始化小助手所在群列表
    inGroupSearch();
}
//小助手所在群上一页
function prevInGropuPage() {
    if(inGroupPageNum>1) {
        inGroupPageNum--;
        var data= getinGroupForm(inGroupPageNum);
        sendInGroupForm(data);
        //alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//小助手所在群一页
function nextInGropuPage() {
    if(inGroupPageNum<parseInt($("#p2-inGropu").text())) {
        inGroupPageNum++;
        var data= getinGroupForm(inGroupPageNum);
        sendInGroupForm(data);
        //alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}
//非空验证
function notNull() {
   if($(this).val()==""){
       $("#error").removeClass('none-box');
       $(this).focus();
   }else {
       $("#error").addClass('none-box');
   }
}

//数据导出页面
//小助手加好友发链接情况报表
function reportsLink() {
    $.ajax({
        type: 'POST',
        url: "",
        data: {
            "time":$("#outStartTime1").val()
        },
        success: function(data){
          alert("小助手加好友发链接情况报表导出成功。");
        }
    });
}
//小助手入群时间明细报表
function reportsTime() {
    $.ajax({
        type: 'POST',
        url: "",
        data: {
            "time":$("#outStartTime2").val()
        },
        success: function(data){
            alert("小助手入群时间明细报表导出成功。");
        }
    });
}
//退出
function dropOut() {
    window.location.href="login.html";
}
//处理数据为null的表格显示问题
function checkNull(arg) {
    if(arg==null){
        return "";
    }else {
        return arg;
    }
}

