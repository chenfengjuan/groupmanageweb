/**
 * Created by chenfengjuan on 16/10/8.
 */
$().ready(function () {
    // 回车键事件
    // 绑定键盘按下事件
    $(document).keydown(function(e) {
        // 回车键事件
        if(e.which == 13) {
            if($("#formLogin").length>0){
                login();
            }
        }
    });
});
//登陆
function login() {
    if($("#userID").val()==""){
        alert("账号不能为空");
    }else if($("#password").val()==""){
        alert("密码不能为空");
    }else {
        $.post("http://wyeth.gemii.cc/HelperManage/login",
            {
                username:$("#userID").val(),
                password:$("#password").val()
            },
            function(data){
                if(data.status==-1){
                    alert(data.msg);
                } else {
                    window.location.href="index.html";
                }

            });
    }
}
